# Import a library of functions called 'pygame'
import pygame
 
# Initialize the game engine
pygame.init()
# Initialize the font
pygame.font.init()

# save the font in a variable
myfont = pygame.font.SysFont('Tahoma', 25)

# Set colors
BLACK = (  0,   0,   0)
WHITE = (255, 255, 255)
 
# Set the height and width of the screen
size = [800, 600]
width, height = size[0], size[1]
screen = pygame.display.set_mode(size)

# prames per second
FPS = 60

# ball class
class ball:
  w, h = 20, 20
  posX, posY, veloX, veloY, dirX, dirY = width / 2 - w / 2, height / 2 - h / 2, 3, 3, 1, 1

# pc class
class pc:
  w, h = 20, 80
  posX = 20

# user class
class user:
  w, h = pc.w, pc.h
  posX = width - pc.posX - w

# set the window label
pygame.display.set_caption("Simple Pong")
 
# 'end of game' flag
done = False

# timing
clock = pygame.time.Clock()

# identifier to the content to be shown
page = 0

# define the Game Over text
gameover = myfont.render('VOCÊ PERDEU!!', False, BLACK)

# get the text's width and height
g_o_text_width, g_o_text_height = gameover.get_width(), gameover.get_height()
 
# game loop
while not done:
  #Loop until the user clicks the close button or tap the 'q' key
  for event in pygame.event.get(): # User did something
    if event.type == pygame.QUIT: # If user clicked close
      done=True

    if pygame.key.get_pressed()[pygame.K_q]:
      done=True
  
  if page == 0:
    #game is still running
    pygame.event.pump()

    mouseX, mouseY = pygame.mouse.get_pos()
    
    # Clear the screen and set the screen background
    screen.fill(BLACK)

    # ball update
    ball.posX += ball.veloX * ball.dirX
    ball.posY += ball.veloY * ball.dirY

    # ball tests
    if ball.posX - ball.w / 2 <= pc.posX + pc.w:
      ball.dirX = -ball.dirX
    
    if ball.posY - ball.h / 2 <= 0 or ball.posY + ball.h / 2 >= height :
      ball.dirY = -ball.dirY

    if ball.posX + ball.w / 2 >= user.posX:
      if ball.posY + ball.h / 2 >= mouseY - user.h / 2 and ball.posY - ball.h / 2 <= mouseY + user.h / 2:
        ball.posX = user.posX - ball.w
        ball.dirX = -ball.dirX
      else:
        page = 1

    # ball
    pygame.draw.rect(screen, WHITE, [ball.posX - ball.w / 2, ball.posY - ball.h / 2, ball.w, ball.h])

    # Draw a solid rectangle
    pygame.draw.rect(screen, WHITE, [pc.posX, ball.posY + ball.h / 2 - pc.h / 2, pc.w, pc.h])

    # Draw a solid rectangle
    pygame.draw.rect(screen, WHITE, [user.posX, mouseY - user.h / 2, user.w, user.h])
    
    # Go ahead and update the screen with what we've drawn.
    # This MUST happen after all the other drawing commands.
  else:
    #you lose
    screen.fill(WHITE)
    screen.blit(gameover,(width / 2 - g_o_text_width / 2, height / 2 - g_o_text_height / 2))

  pygame.display.flip()

  # This limits the while loop to a max of 10 times per second.
  # Leave this out and we will use all CPU we can.
  clock.tick(FPS)
# Be IDLE friendly
pygame.quit()